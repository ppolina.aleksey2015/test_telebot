import requests
from config import HOTELS_TOKEN, TELEGRAM_TOKEN
import buffers

import db

from telebot import TeleBot
from telebot.types import InputMediaPhoto

bot = TeleBot(TELEGRAM_TOKEN)


def get_region_id(city: str):
    url = "https://hotels4.p.rapidapi.com/locations/v3/search"

    querystring = {"q": city.title(), "locale": "ru_RU"}

    headers = {
        "X-RapidAPI-Key": HOTELS_TOKEN,
        "X-RapidAPI-Host": "hotels4.p.rapidapi.com"
    }

    try:
        response = requests.get(url, headers=headers, params=querystring).json()
        return response['sr'][0]['gaiaId']

    except:
        return None


def get_hotels(id):
    data = buffers.buffer[id]

    year_in = int(data['checkIn'].split('.')[0])
    moth_in = int(data['checkIn'].split('.')[1])
    day_in = int(data['checkIn'].split('.')[2])

    year_out = int(data['checkOut'].split('.')[0])
    moth_out = int(data['checkOut'].split('.')[1])
    day_out = int(data['checkOut'].split('.')[2])

    url = "https://hotels4.p.rapidapi.com/properties/v2/list"

    if data['command'] != '/bestdeal':
        payload = {
            "currency": "USD",
            "eapid": 1,
            "locale": "en_US",
            "siteId": 300000001,
            "destination": {
                "regionId": data['city']
            },
            "checkInDate": {
                "day": 10,
                "month": 10,
                "year": 2022
            },
            "checkOutDate": {
                "day": 15,
                "month": 10,
                "year": 2022
            },
            "rooms": [
                {
                    "adults": 1
                }
            ],
            "resultsStartingIndex": 0,
            "resultsSize": 200,
            "sort": data['sort']}
    else:
        payload = {
            "currency": "USD",
            "eapid": 1,
            "locale": "en_US",
            "siteId": 300000001,
            "destination": {"regionId": data['city']},
            "checkInDate": {
                "day": 10,
                "month": 10,
                "year": 2022
            },
            "checkOutDate": {
                "day": 15,
                "month": 10,
                "year": 2022
            },
            "rooms": [{"adults": 1}],
            "resultsStartingIndex": 0,
            "resultsSize": 200,
            "sort": "DISTANCE",
            "filters": {"price": {
                "max": data['max_p'],
                "min": data['min_p']
            }}
        }

    headers = {
        "content-type": "application/json",
        "X-RapidAPI-Key": HOTELS_TOKEN,
        "X-RapidAPI-Host": "hotels4.p.rapidapi.com"
    }

    try:
        response = requests.post(url, json=payload, headers=headers).json()
        properties = response['data']['propertySearch']['properties']

        return properties
    except Exception as e:
        print(e)
        return None


def get_detail(hotel_id):
    url = "https://hotels4.p.rapidapi.com/properties/v2/detail"

    payload = {
        "currency": "RUB",
        "locale": "ru_RU",
        "propertyId": hotel_id
    }
    headers = {
        "content-type": "application/json",
        "X-RapidAPI-Key": HOTELS_TOKEN,
        "X-RapidAPI-Host": "hotels4.p.rapidapi.com"
    }

    response = requests.post(url, json=payload, headers=headers).json()

    return response['data']['propertyInfo']


def send_hotels(id):
    try:
        data = buffers.buffer[id]
        hotels = list(filter(check, get_hotels(id)))

        if data['command'] == '/highprice':
            hotels = hotels[-data['hotels_count']:]

        if not hotels:
            bot.send_message(id, 'Не удалось найти подходящие отели \n\n')
            bot.send_message(id, 'Введите команду /start для запуска бота.')
            return

        print(len(hotels))

        names_of_hotel = []

        count_of_result = 0
        for hotel in hotels:
            if count_of_result >= data['hotels_count']:
                break

            name = hotel['name']
            price = hotel['price']['lead']['formatted']
            distance = float(hotel['destinationInfo']['distanceFromDestination']['value'])
            total_price = int(hotel['price']['lead']['amount']) * data['delta']

            if data['command'] == '/bestdeal':
                if float(data['min_distance']) > distance or float(data['max_distance']) < distance:
                    print(distance)
                    print(price)
                    continue

            detail = get_detail(hotel['id'])

            adress = detail['summary']['location']['address']['addressLine']
            url = f'https://www.hotels.com/h{hotel["id"]}.Hotel-Information'

            msg = f'*{name}* \n\n' \
                  f'Адрес: *{adress}*\n' \
                  f'Расстояние до центра *{str(distance)} км* \n\n' \
                  f'Цена за ночь: *{price}* \n' \
                  f'Сумма для указанных дат: *${total_price}* \n' \
                  f'Ссылка на отель для более подробной информации: {url}'

            if buffers.buffer[id]['load_photo']:
                photo_urls = [image_box['image']['url'] for image_box in detail['propertyGallery']['images']][
                             :buffers.buffer[id]['photo_count']]
                media = []

                count = 0
                for url in photo_urls:
                    if count == 0:
                        media.append(InputMediaPhoto(media=url, caption=msg, parse_mode='MARKDOWN'))
                    else:
                        media.append(InputMediaPhoto(media=url))
                    count += 1

                bot.send_media_group(id, media)
            else:
                bot.send_message(id, msg, parse_mode='MARKDOWN')

            url = f'https://www.hotels.com/h{hotel["id"]}.Hotel-Information'

            names_of_hotel.append(f'[{name}]({url})')
            count_of_result += 1

        if count_of_result > 0:
            note = f'{data["command"]} --- {data["time"]} --- {", ".join(names_of_hotel)}'
            past = db.get_data(id=id)[0][1].split(';')
            past.append(note)
            db.update_data(data={'history': ';'.join(past)}, id=id)

            if count_of_result < data['hotels_count']:
                bot.send_message(id, f'По вашему запросу было найдено {count_of_result} подходящих отелей,'
                                     f' вместо указанных вами {data["hotels_count"]}')

            bot.send_message(id, 'Введите команду /start для запуска бота.')


        else:
            bot.send_message(id, 'Не удалось найти подходящие отели \n\n')
            bot.send_message(id, 'Введите команду /start для запуска бота.')
    except Exception as e:
        print(e)
        bot.send_message(id, 'Произошла непредвиденная ошибка. Попробуйте снова')
        bot.send_message(id, 'Введите команду /start для запуска бота.')


def check(id_data):
    if id_data['price']['lead']['formatted'] != '$0':
        return True
    else:
        return False
