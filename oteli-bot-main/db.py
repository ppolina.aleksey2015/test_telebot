import sqlite3


def register(message):
    insert_data = [(message.from_user.id, '')]

    with sqlite3.connect('database/database.db') as db:
        cursor = db.cursor()

        query = '''INSERT INTO users(id, history) VALUES(?, ?) '''

        cursor.executemany(query, insert_data)
        db.commit()


def update_data(data: dict(), id=None, table='users'):
    '''
    Обновление данных в бд

    data - словарь в формате <колонна>:<значение> (можно несколько)
    id - обновление произойдет только у определенного пользователя, если None - у всех
    table - таблица, в которой произойдет обновление данных
    '''

    with sqlite3.connect('database/database.db') as db:
        cursor = db.cursor()

        if id != None:
            for key in data:
                update_data = (data[key], id)

                query = f'UPDATE {table} SET {key} = ? where id = ?'
                cursor.execute(query, update_data)

        else:
            for key in data:
                update_data = (data[key],)

                query = f'UPDATE {table} SET {key} = ?'
                cursor.execute(query, update_data)

        db.commit()


def get_data(id=None, table='users'):
    '''
    Получает данные из бд

    user_id - поиск осуществится по айди юзера, если None возвращаются все данные
    table - поиск произойдет в указанной таблице
    '''

    with sqlite3.connect('database/database.db') as db:
        cursor = db.cursor()

        if id != None:
            select_query = f'SELECT * from {table} where id = ?'
            cursor.execute(select_query, (id,))

        if id == None:
            select_query = f'SELECT * from {table}'
            cursor.execute(select_query)

        data = cursor.fetchall()

        return data
